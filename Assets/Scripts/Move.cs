﻿using UnityEngine;

public class Move : MonoBehaviour
{
    public float forceValue = 5;
    public float jumpValue;
    private Rigidbody rigidbody;
    private AudioSource audio;
    public int vida = 100;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") && Mathf.Abs(rigidbody.velocity.y) < 0.01f)
        {
            rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
            audio.Play();
        }

        if (Input.touchCount == 1)
        {
            if (Input.touches[0].phase == TouchPhase.Began && Mathf.Abs(rigidbody.velocity.y) < 0.01f)
            {
                rigidbody.AddForce(Vector3.up * jumpValue, ForceMode.Impulse);
                audio.Play();
            }
        }
        
    }

    private void FixedUpdate()
    {
        rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")) * forceValue);
        rigidbody.AddForce(new Vector3(Input.acceleration.x, 0, Input.acceleration.y) * forceValue);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            print("Colisión");
            //Destroy(collision.gameObject);
            vida--;
            if (vida <= 0)
            {
                print("La esfera ha muerto");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        print("Entras en la zona oscura");
    }
}
