﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Eruption : MonoBehaviour
{
    public GameObject stone1;
    public GameObject stone2;
    public float fireRate = 0.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ThrowStone());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    IEnumerator ThrowStone()
    {
        yield return new WaitForSeconds(2.0f);
        
        while (true)
        {
            Instantiate(stone1, transform.position, Random.rotation);
            Instantiate(stone2, transform.position, Random.rotation);
            yield return new WaitForSeconds(fireRate);
        }
    }
}
